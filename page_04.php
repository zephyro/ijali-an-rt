<!doctype html>
<html class="no-js" lang="">
    <head>
        <?php include('inc/head.inc.php') ?>
    </head>
    <body>


        <div class="page">

            <?php include('inc/header.inc.php') ?>

            <nav class="nav">
                <div class="container">
                    <ul class="nav__menu">
                        <li><a href="#"><span>Проекты</span></a></li>
                        <li><a href="#"><span>Файлы</span></a></li>
                        <li><a href="#"><span>Рабочая область</span></a></li>
                        <li><a href="#"><span>Редактирование аффиксов</span></a></li>
                        <li class="active"><a href="#"><span>Редактирование словаря</span></a></li>
                    </ul>
                </div>
            </nav>


            <section class="main">
                <div class="container">

                    <div class="heading">
                        <a href="#" class="btn btn_brown hide_bar_toggle">Добавить слово</a>
                        <div class="heading__search">
                            <div class="search search_inline">
                                <input type="text" class="form_control search__input" name="search" placeholder="Поиск..." value="">
                                <button type="button" class="search__btn"></button>
                                <div class="search__nav">
                                    <div class="search__nav_toggle">
                                        <span></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="hide_bar">
                        <div class="hide_bar__row">
                            <div class="hide_bar__item hide_bar__item_lg">
                                <div class="form_label">Арабское написание:</div>
                                <input class="form_control" type="text" name="" value="" placeholder="">
                            </div>
                            <div class="hide_bar__item hide_bar__item_lg">
                                <div class="form_label">Татарский:</div>
                                <input class="form_control" type="text" name="" value="" placeholder="">
                            </div>
                            <div class="hide_bar__item hide_bar__item_lg">
                                <div class="form_label">Татарский описание:</div>
                                <input class="form_control" type="text" name="" value="" placeholder="">
                            </div>
                            <div class="hide_bar__item hide_bar__item_lg">
                                <div class="form_label">Русский:</div>
                                <input class="form_control" type="text" name="" value="" placeholder="">
                            </div>
                        </div>
                        <button class="btn btn_brown hide_bar_submit" type="button">Сохранить</button>
                    </div>

                    <table class="table">
                        <tr>
                            <th>Арабский:</th>
                            <th>Татарский(произношение):</th>
                            <th>Татарский:</th>
                            <th>Русский:</th>
                            <th></th>
                        </tr>
                        <tr>
                            <td>ابابيل</td>
                            <td>әбабил</td>
                            <td>Көрәшә, яр карлыгачы.</td>
                            <td>Стриж, береговая ласточка.</td>
                            <td>
                                <button class="btn_edit"></button>
                                <button class="btn_delete"></button>
                            </td>
                        </tr>
                        <tr>
                            <td>ابابيل</td>
                            <td>әбабил</td>
                            <td>Көрәшә, яр карлыгачы.</td>
                            <td>Стриж, береговая ласточка.</td>
                            <td>
                                <button class="btn_edit"></button>
                                <button class="btn_delete"></button>
                            </td>
                        </tr>
                        <tr>
                            <td>ابابيل</td>
                            <td>әбабил</td>
                            <td>Көрәшә, яр карлыгачы.</td>
                            <td>Стриж, береговая ласточка.</td>
                            <td>
                                <button class="btn_edit"></button>
                                <button class="btn_delete"></button>
                            </td>
                        </tr>
                        <tr>
                            <td>ابابيل</td>
                            <td>әбабил</td>
                            <td>Көрәшә, яр карлыгачы.</td>
                            <td>Стриж, береговая ласточка.</td>
                            <td>
                                <button class="btn_edit"></button>
                                <button class="btn_delete"></button>
                            </td>
                        </tr>
                        <tr>
                            <td>ابابيل</td>
                            <td>әбабил</td>
                            <td>Көрәшә, яр карлыгачы.</td>
                            <td>Стриж, береговая ласточка.</td>
                            <td>
                                <button class="btn_edit"></button>
                                <button class="btn_delete"></button>
                            </td>
                        </tr>
                        <tr>
                            <td>ابابيل</td>
                            <td>әбабил</td>
                            <td>Көрәшә, яр карлыгачы.</td>
                            <td>Стриж, береговая ласточка.</td>
                            <td>
                                <button class="btn_edit"></button>
                                <button class="btn_delete"></button>
                            </td>
                        </tr>
                        <tr>
                            <td>ابابيل</td>
                            <td>әбабил</td>
                            <td>Көрәшә, яр карлыгачы.</td>
                            <td>Стриж, береговая ласточка.</td>
                            <td>
                                <button class="btn_edit"></button>
                                <button class="btn_delete"></button>
                            </td>
                        </tr>
                        <tr>
                            <td>ابابيل</td>
                            <td>әбабил</td>
                            <td>Көрәшә, яр карлыгачы.</td>
                            <td>Стриж, береговая ласточка.</td>
                            <td>
                                <button class="btn_edit"></button>
                                <button class="btn_delete"></button>
                            </td>
                        </tr>
                        <tr>
                            <td>ابابيل</td>
                            <td>әбабил</td>
                            <td>Көрәшә, яр карлыгачы.</td>
                            <td>Стриж, береговая ласточка.</td>
                            <td>
                                <button class="btn_edit"></button>
                                <button class="btn_delete"></button>
                            </td>
                        </tr>
                        <tr>
                            <td>ابابيل</td>
                            <td>әбабил</td>
                            <td>Көрәшә, яр карлыгачы.</td>
                            <td>Стриж, береговая ласточка.</td>
                            <td>
                                <button class="btn_edit"></button>
                                <button class="btn_delete"></button>
                            </td>
                        </tr>
                    </table>

                </div>
            </section>

            <?php include('inc/footer.inc.php') ?>

        </div>

        <?php include('inc/scripts.inc.php') ?>

    </body>
</html>
