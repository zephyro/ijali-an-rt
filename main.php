<!doctype html>
<html class="no-js" lang="">
    <head>
        <?php include('inc/head.inc.php') ?>
    </head>
    <body>


        <div class="page">

            <?php include('inc/header.inc.php') ?>

            <section class="primary">
                <div class="container">
                    <h1 class="primary__heading">
                        <strong>Система распознавания и <br/>транслитерации</strong>
                        <span>старо-татарского(арабского) текста</span>
                    </h1>
                    <nav class="primary__nav">
                        <ul class="nav__menu">
                            <li class="active"><a href="#"><span>Проекты</span></a></li>
                            <li><a href="#"><span>Файлы</span></a></li>
                            <li><a href="#"><span>Рабочая область</span></a></li>
                            <li><a href="#"><span>Редактирование аффиксов</span></a></li>
                            <li><a href="#"><span>Редактирование словаря</span></a></li>
                        </ul>
                    </nav>
                </div>
            </section>

            <section class="main">
                <div class="container">
                    <div class="row">
                        <div class="col_3">
                            <a class="directory" href="#">
                                <div class="directory__title">Современное западное мировоззрение и восточные религии</div>
                                <div class="directory__link"><span>Перейти</span></div>
                                <div class="directory__files">
                                    <strong>46</strong>
                                    <span>файлов</span>
                                </div>
                            </a>
                        </div>
                        <div class="col_3">
                            <a class="directory" href="#">
                                <div class="directory__title">Мессианские аспекты нагорной проповеди.</div>
                                <div class="directory__link"><span>Перейти</span></div>
                                <div class="directory__files">
                                    <strong>6</strong>
                                    <span>файлов</span>
                                </div>
                            </a>
                        </div>
                        <div class="col_3">
                            <a class="directory" href="#">
                                <div class="directory__title">Правовой статус традиционной религии</div>
                                <div class="directory__link"><span>Перейти</span></div>
                                <div class="directory__files">
                                    <strong>14</strong>
                                    <span>файлов</span>
                                </div>
                            </a>
                        </div>
                        <div class="col_3">
                            <a class="directory" href="#">
                                <div class="directory__title">Свт. Иоанн Златоуст о необходимости апологетики</div>
                                <div class="directory__link"><span>Перейти</span></div>
                                <div class="directory__files">
                                    <strong>63</strong>
                                    <span>файлов</span>
                                </div>
                            </a>
                        </div>
                        <div class="col_3">
                            <a class="directory" href="#">
                                <div class="directory__title">Богослужебные тексты о еретиках</div>
                                <div class="directory__link"><span>Перейти</span></div>
                                <div class="directory__files">
                                    <strong>12</strong>
                                    <span>файлов</span>
                                </div>
                            </a>
                        </div>
                        <div class="col_3">
                            <a class="directory" href="#">
                                <div class="directory__title">Смиренный весельчак Амвросий</div>
                                <div class="directory__link"><span>Перейти</span></div>
                                <div class="directory__files">
                                    <strong>8</strong>
                                    <span>файлов</span>
                                </div>
                            </a>
                        </div>
                        <div class="col_3">
                            <a class="directory" href="#">
                                <div class="directory__title">Истинность наших представлений о Боге</div>
                                <div class="directory__link"><span>Перейти</span></div>
                                <div class="directory__files">
                                    <strong>2</strong>
                                    <span>файлов</span>
                                </div>
                            </a>
                        </div>
                        <div class="col_3">
                            <a class="directory" href="#">
                                <div class="directory__title">Йога в Греции</div>
                                <div class="directory__link"><span>Перейти</span></div>
                                <div class="directory__files">
                                    <strong>11</strong>
                                    <span>файлов</span>
                                </div>
                            </a>
                        </div>
                        <div class="col_3">
                            <a class="directory" href="#">
                                <div class="directory__title">Старец Паисий Святогорец о реинкарнации и индуизме</div>
                                <div class="directory__link"><span>Перейти</span></div>
                                <div class="directory__files">
                                    <strong>7</strong>
                                    <span>файлов</span>
                                </div>
                            </a>
                        </div>
                        <div class="col_3">
                            <a class="directory" href="#">
                                <div class="directory__title">Святейший Патриарх Сербский ПАВЕЛ</div>
                                <div class="directory__link"><span>Перейти</span></div>
                                <div class="directory__files">
                                    <strong>33</strong>
                                    <span>файлов</span>
                                </div>
                            </a>
                        </div>
                        <div class="col_3">
                            <a class="directory" href="#">
                                <div class="directory__title">Николай Японский о памятнике Пушкину</div>
                                <div class="directory__link"><span>Перейти</span></div>
                                <div class="directory__files">
                                    <strong>22</strong>
                                    <span>файлов</span>
                                </div>
                            </a>
                        </div>
                        <div class="col_3">
                            <a class="directory" href="#">
                                <div class="directory__title">Аум Синрикё запретили, что дальше?</div>
                                <div class="directory__link"><span>Перейти</span></div>
                                <div class="directory__files">
                                    <strong>91</strong>
                                    <span>файлов</span>
                                </div>
                            </a>
                        </div>
                        <div class="col_3">
                            <a class="directory" href="#">
                                <div class="directory__title">Современные технологии вербовки в секты</div>
                                <div class="directory__link"><span>Перейти</span></div>
                                <div class="directory__files">
                                    <strong>12</strong>
                                    <span>файлов</span>
                                </div>
                            </a>
                        </div>
                        <div class="col_3">
                            <a class="directory" href="#">
                                <div class="directory__title">Лидер группы Metallica о своем сектантском детстве</div>
                                <div class="directory__link"><span>Перейти</span></div>
                                <div class="directory__files">
                                    <strong>8</strong>
                                    <span>файлов</span>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
            </section>

            <?php include('inc/footer.inc.php') ?>

        </div>

        <?php include('inc/scripts.inc.php') ?>

    </body>
</html>
