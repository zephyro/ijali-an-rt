<!doctype html>
<html class="no-js" lang="">
    <head>
        <?php include('inc/head.inc.php') ?>
    </head>
    <body>


        <div class="page">

            <?php include('inc/header.inc.php') ?>

            <nav class="nav">
                <div class="container">
                    <ul class="nav__menu">
                        <li><a href="#"><span>Проекты</span></a></li>
                        <li class="active"><a href="#"><span>Файлы</span></a></li>
                        <li><a href="#"><span>Рабочая область</span></a></li>
                        <li><a href="#"><span>Редактирование аффиксов</span></a></li>
                        <li><a href="#"><span>Редактирование словаря</span></a></li>
                    </ul>
                </div>
            </nav>


            <section class="main">
                <div class="container">
                    <h1>Современное западное мировоззрение и восточные религии</h1>
                    <div class="main_author"><span>Автор: </span> <a href="#">Белицкая Оксана</a></div>
                    <div class="heading">
                        <div class="heading__title">Загруженные файлы:</div>
                        <div class="heading__actions">
                            <a href="#" class="btn_icon btn_icon_download"></a>
                            <a href="#" class="btn_icon btn_icon_reload"></a>
                        </div>
                    </div>

                    <ul class="pagination">
                        <li>
                            <a href="#" class="pagination_prev">
                                <i>
                                    <svg xmlns="http://www.w3.org/2000/svg" width="7.449" height="13.898" viewBox="0 0 7.449 13.898" class="ico_svg">
                                        <path d="M4427.9,423l-6.242,6.242,5.461,5.462.78.78" transform="translate(-4421.163 -422.293)" fill="none" stroke-linecap="round" stroke-linejoin="round" stroke-width="1"/>
                                    </svg>
                                </i>
                            </a>
                        </li>
                        <li>
                            <a href="#">1</a>
                        </li>
                        <li class="active">
                            <a href="#">2</a>
                        </li>
                        <li>
                            <a href="#">3</a>
                        </li>
                        <li>
                            <a href="#">4</a>
                        </li>
                        <li>
                            <a href="#">5</a>
                        </li>
                        <li><span>...</span></li>
                        <li>
                            <a href="#">66</a>
                        </li>
                        <li>
                            <a href="#">67</a>
                        </li>
                        <li>
                            <a href="#" class="pagination_next">
                                <i>
                                    <svg xmlns="http://www.w3.org/2000/svg" width="7.449" height="13.898" viewBox="0 0 7.449 13.898" class="ico_svg">
                                        <path d="M4427.9,423l-6.242,6.242,5.461,5.462.78.78" transform="translate(-4421.163 -422.293)" fill="none" stroke-linecap="round" stroke-linejoin="round" stroke-width="1"/>
                                    </svg>
                                </i>
                            </a>
                        </li>
                    </ul>

                    <div class="board_row">

                        <div class="board">
                            <div class="board__heading">
                                <div class="board__heading_title">Изображение</div>
                            </div>
                            <div class="board__box">
                                <div class="board__top">
                                    <div class="board__zoom">
                                        <button class="btn_zoom btn_zoom_plus"></button>
                                        <button class="btn_zoom btn_zoom_minus"></button>
                                    </div>
                                </div>
                                <div class="board__content">
                                    <div class="board__scroll board__scroll_left">
                                        <div class="board__image">
                                            <img src="images/scan.jpg" class="board_img" alt="">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="board">
                            <div class="board__heading">
                                <div class="board__heading_title">Арабский текст:</div>
                                <a href="#" class="board__heading_toggle board__heading_show">показать блок</a>
                            </div>
                            <div class="board__box">
                                <div class="board__top">
                                    <div class="board__buttons">
                                        <button class="btn_save"></button>
                                        <button class="btn_nav btn_nav_prev"></button>
                                        <button class="btn_nav btn_nav_next"></button>
                                    </div>
                                    <a href="#" class="board__link">Перевести на кириллицу</a>
                                </div>
                                <div class="board__content board__content_right">
                                    <div class="board__scroll board__scroll_right">
                                        <div class="board__text">
                                            قبس ا اآ ك<br/>
                                            هكە۱٩؟ صر ە  ‎ ۱»«‏ و . )<br/>
                                            ل۱ همارن اەليوزا ‏ نم<br/>
                                            ر)١ا ۱‏ ەلفڭ ً۴ا. ‏ ا{) و.ّ سستن بي .هل. ‏<br/>
                                            وفييمريك نوەی ەوور ك چ اال۱نللڭاا للاەاەەەەەەەە ەي ‏ ەەيەهەەهەەەيەلوەەتكلەي ەلنەتەنەلەەەەلنەلە«اا .۵‏<br/>
                                            ك ( انرنەي. ‏ قنّ:ل۹-ك ادن بيش كونددە بر چققان ادلى سياسى جمو عەدر.<br/>
                                            رنرنولا ‏ .با . .كك يمەالڭّ ۱ كعحەەەتەعەعەەەەەمەعەعمەەك وەتعەەەەەتە تتەعەەععەنەەعەەدناك<br/>
                                            ع ەە :«ا ‏ :.<br/>
                                            د ‏ حرە (لو ‏<br/>
                                            نرزوّ آّ ی<br/>
                                            ل. ّ ر ممم<br/>
                                            _ ر ‎ )‏ نوا لي اننم، .<br/>
                                            ك _ ننەکا يڭن آق ‎.H.‏ تو شمکا پلرن کكڭ يار ۱ا تقا نم<br/>
                                            زونوی بيشگ يی الڭە يگ ننچو يمه:۱ا ۱‏ هيكڭ ا ر ا تقان<br/>
                                            !. !۱ ۳قزنين ۱‏ چن زريغفا بو بدرڭ يهر ‏ ڭزو زد زرنی:. )_<br/>
                                            چهچكا يیلرن اياقق بلف تيڭە ياراتقان ! و:.<br/>
                                            _ ك دتقر دك هل<br/>
                                            _ بج:و:.۱ ‎ .:)‏ دتری<br/>
                                            _ ر ‎ )‏ نوا لي اننم، .<br/>
                                            قبس ا اآ ك<br/>
                                            هكە۱٩؟ صر ە  ‎ ۱»«‏ و . )<br/>
                                            ل۱ همارن اەليوزا ‏ نم<br/>
                                            ر)١ا ۱‏ ەلفڭ ً۴ا. ‏ ا{) و.ّ سستن بي .هل. ‏<br/>
                                            وفييمريك نوەی ەوور ك چ اال۱نللڭاا للاەاەەەەەەەە ەي ‏ ەەيەهەەهەەەيەلوەەتكلەي ەلنەتەنەلەەەەلنەلە«اا .۵‏<br/>
                                            ك ( انرنەي. ‏ قنّ:ل۹-ك ادن بيش كونددە بر چققان ادلى سياسى جمو عەدر.<br/>
                                            رنرنولا ‏ .با . .كك يمەالڭّ ۱ كعحەەەتەعەعەەەەەمەعەعمەەك وەتعەەەەەتە تتەعەەععەنەەعەەدناك<br/>
                                            ع ەە :«ا ‏ :.<br/>
                                            د ‏ حرە (لو ‏<br/>
                                            نرزوّ آّ ی<br/>
                                            ل. ّ ر ممم<br/>
                                            _ ر ‎ )‏ نوا لي اننم، .<br/>
                                            ك _ ننەکا يڭن آق ‎.H.‏ تو شمکا پلرن کكڭ يار ۱ا تقا نم<br/>
                                            زونوی بيشگ يی الڭە يگ ننچو يمه:۱ا ۱‏ هيكڭ ا ر ا تقان<br/>
                                            !. !۱ ۳قزنين ۱‏ چن زريغفا بو بدرڭ يهر ‏ ڭزو زد زرنی:. )_<br/>
                                            چهچكا يیلرن اياقق بلف تيڭە ياراتقان ! و:.<br/>
                                            _ ك دتقر دك هل<br/>
                                            _ بج:و:.۱ ‎ .:)‏ دتری<br/>
                                            _ ر ‎ )‏ نوا لي اننم، .<br/>
                                            ك _ ننەکا يڭن آق ‎.H.‏ تو شمکا پلرن کكڭ يار ۱ا تقا نم<br/>
                                            زونوی بيشگ يی الڭە يگ ننچو يمه:۱ا ۱‏ هيكڭ ا ر ا تقان<br/>
                                            !. !۱ ۳قزنين ۱‏ چن زريغفا بو بدرڭ يهر ‏ ڭزو زد زرنی:. )_<br/>
                                            چهچكا يیلرن اياقق بلف تيڭە ياراتقان ! و:.<br/>
                                            _ ك دتقر دك هل<br/>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="board board_last">
                            <div class="board__heading">
                                <div class="board__heading_title">Кириллица:</div>
                                <a href="#" class="board__heading_toggle board__heading_hide">скрыть блок</a>
                            </div>
                            <div class="board__box">
                                <div class="board__top">
                                    <div class="board__buttons">
                                        <button class="btn_save"></button>
                                        <button class="btn_nav btn_nav_prev"></button>
                                        <button class="btn_nav btn_nav_next"></button>
                                    </div>
                                </div>
                                <div class="board__content">
                                    <div class="board__scroll board__scroll_left">
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>

                </div>
            </section>

            <?php include('inc/footer.inc.php') ?>

        </div>

        <?php include('inc/scripts.inc.php') ?>

        <script>
            $(".board__scroll").mCustomScrollbar({
                axis: "xy",
                theme:"dark",
           //     autoHideScrollbar: true,
                scrollbarPosition:"outside"
            });
        </script>

    </body>
</html>
