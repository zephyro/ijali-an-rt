<!doctype html>
<html class="no-js" lang="">
    <head>
        <?php include('inc/head.inc.php') ?>
    </head>
    <body>


        <div class="page">

            <?php include('inc/header.inc.php') ?>

            <nav class="nav">
                <div class="container">
                    <ul class="nav__menu">
                        <li><a href="#"><span>Проекты</span></a></li>
                        <li><a href="#"><span>Файлы</span></a></li>
                        <li><a href="#"><span>Рабочая область</span></a></li>
                        <li class="active"><a href="#"><span>Редактирование аффиксов</span></a></li>
                        <li><a href="#"><span>Редактирование словаря</span></a></li>
                    </ul>
                </div>
            </nav>


            <section class="main">
                <div class="container">

                    <div class="heading">
                        <a href="#" class="btn btn_brown hide_bar_toggle">Добавить аффикс</a>
                        <div class="heading__search">
                            <div class="search">
                                <input type="text" class="form_control search__input" name="search" placeholder="Поиск..." value="">
                                <button type="button" class="search__btn"></button>
                            </div>
                        </div>
                    </div>

                    <div class="hide_bar">
                        <div class="hide_bar__row">
                            <div class="hide_bar__item hide_bar__item_md">
                                <div class="form_label">Арабское написание:</div>
                                <input class="form_control" type="text" name="" value="" placeholder="">
                            </div>
                            <div class="hide_bar__item hide_bar__item_sm">
                                <div class="form_label">Код UTF8:</div>
                                <input class="form_control" type="text" name="" value="" placeholder="">
                            </div>
                            <div class="hide_bar__item hide_bar__item_sm">
                                <div class="form_label">ТГ:</div>
                                <input class="form_control" type="text" name="" value="" placeholder="">
                            </div>
                            <div class="hide_bar__item hide_bar__item_sm">
                                <div class="form_label">МГ</div>
                                <input class="form_control" type="text" name="" value="" placeholder="">
                            </div>
                            <div class="hide_bar__item hide_bar__item_sm">
                                <div class="form_label">ТГ+ЗС</div>
                                <input class="form_control" type="text" name="" value="" placeholder="">
                            </div>
                            <div class="hide_bar__item hide_bar__item_sm">
                                <div class="form_label">ТГ+ГС</div>
                                <input class="form_control" type="text" name="" value="" placeholder="">
                            </div>
                            <div class="hide_bar__item hide_bar__item_sm">
                                <div class="form_label">МГ+ЗС</div>
                                <input class="form_control" type="text" name="" value="" placeholder="">
                            </div>
                            <div class="hide_bar__item hide_bar__item_sm">
                                <div class="form_label">МГ+ГС</div>
                                <input class="form_control" type="text" name="" value="" placeholder="">
                            </div>
                            <div class="hide_bar__item hide_bar__item_sm">
                                <div class="form_label">ЗС+Ъ</div>
                                <input class="form_control" type="text" name="" value="" placeholder="">
                            </div>
                            <div class="hide_bar__item hide_bar__item_sm">
                                <div class="form_label">ЗС+Ь</div>
                                <input class="form_control" type="text" name="" value="" placeholder="">
                            </div>
                            <div class="hide_bar__item hide_bar__item_sm">
                                <div class="form_label">ГС+Ъ</div>
                                <input class="form_control" type="text" name="" value="" placeholder="">
                            </div>
                            <div class="hide_bar__item hide_bar__item_sm">
                                <div class="form_label">ГС+Ь</div>
                                <input class="form_control" type="text" name="" value="" placeholder="">
                            </div>
                        </div>
                        <button class="btn btn_brown hide_bar_submit" type="button">Сохранить</button>
                    </div>

                    <table class="table">
                        <tr>
                            <th></th>
                            <th>UTF#</th>
                            <th>ТГ</th>
                            <th>МГ</th>
                            <th>ТГ+ЗС</th>
                            <th>ТГ+ГС</th>
                            <th>МГ+ЗС</th>
                            <th>МГ+ГС</th>
                            <th>ЗС+Ъ</th>
                            <th>ЗС+Ь</th>
                            <th>ГС+Ъ</th>
                            <th>ГС+Ь</th>
                            <th></th>
                        </tr>
                        <tr>
                            <td>لار</td>
                            <td>| 1604 | 1575 | 1585 |</td>
                            <td>лар</td>
                            <td>лар</td>
                            <td>лар</td>
                            <td>лар</td>
                            <td>лар</td>
                            <td>лар</td>
                            <td>лар</td>
                            <td>лар</td>
                            <td>лар</td>
                            <td>лар</td>
                            <td>
                                <button class="btn_edit"></button>
                                <button class="btn_delete"></button>
                            </td>
                        </tr>
                        <tr>
                            <td>لار</td>
                            <td>| 1604 | 1575 |</td>
                            <td>лар</td>
                            <td>лар</td>
                            <td>лар</td>
                            <td>лар</td>
                            <td>лар</td>
                            <td>лар</td>
                            <td>лар</td>
                            <td>лар</td>
                            <td>лар</td>
                            <td>лар</td>
                            <td>
                                <button class="btn_edit"></button>
                                <button class="btn_delete"></button>
                            </td>
                        </tr>
                        <tr>
                            <td>لار</td>
                            <td>| 1604 |</td>
                            <td>лар</td>
                            <td>лар</td>
                            <td>лар</td>
                            <td>лар</td>
                            <td>лар</td>
                            <td>лар</td>
                            <td>лар</td>
                            <td>лар</td>
                            <td>лар</td>
                            <td>лар</td>
                            <td>
                                <button class="btn_edit"></button>
                                <button class="btn_delete"></button>
                            </td>
                        </tr>
                        <tr>
                            <td>لار</td>
                            <td>| 1604 | 1575 | 1585 |</td>
                            <td>лар</td>
                            <td>лар</td>
                            <td>лар</td>
                            <td>лар</td>
                            <td>лар</td>
                            <td>лар</td>
                            <td>лар</td>
                            <td>лар</td>
                            <td>лар</td>
                            <td>лар</td>
                            <td>
                                <button class="btn_edit"></button>
                                <button class="btn_delete"></button>
                            </td>
                        </tr>
                        <tr>
                            <td>لار</td>
                            <td>| 1604 | 1575 |</td>
                            <td>лар</td>
                            <td>лар</td>
                            <td>лар</td>
                            <td>лар</td>
                            <td>лар</td>
                            <td>лар</td>
                            <td>лар</td>
                            <td>лар</td>
                            <td>лар</td>
                            <td>лар</td>
                            <td>
                                <button class="btn_edit"></button>
                                <button class="btn_delete"></button>
                            </td>
                        </tr>
                        <tr>
                            <td>لار</td>
                            <td>| 1604 |</td>
                            <td>лар</td>
                            <td>лар</td>
                            <td>лар</td>
                            <td>лар</td>
                            <td>лар</td>
                            <td>лар</td>
                            <td>лар</td>
                            <td>лар</td>
                            <td>лар</td>
                            <td>лар</td>
                            <td>
                                <button class="btn_edit"></button>
                                <button class="btn_delete"></button>
                            </td>
                        </tr>
                        <tr>
                            <td>لار</td>
                            <td>| 1604 | 1575 | 1585 |</td>
                            <td>лар</td>
                            <td>лар</td>
                            <td>лар</td>
                            <td>лар</td>
                            <td>лар</td>
                            <td>лар</td>
                            <td>лар</td>
                            <td>лар</td>
                            <td>лар</td>
                            <td>лар</td>
                            <td>
                                <button class="btn_edit"></button>
                                <button class="btn_delete"></button>
                            </td>
                        </tr>
                        <tr>
                            <td>لار</td>
                            <td>| 1604 | 1575 |</td>
                            <td>лар</td>
                            <td>лар</td>
                            <td>лар</td>
                            <td>лар</td>
                            <td>лар</td>
                            <td>лар</td>
                            <td>лар</td>
                            <td>лар</td>
                            <td>лар</td>
                            <td>лар</td>
                            <td>
                                <button class="btn_edit"></button>
                                <button class="btn_delete"></button>
                            </td>
                        </tr>
                        <tr>
                            <td>لار</td>
                            <td>| 1604 |</td>
                            <td>лар</td>
                            <td>лар</td>
                            <td>лар</td>
                            <td>лар</td>
                            <td>лар</td>
                            <td>лар</td>
                            <td>лар</td>
                            <td>лар</td>
                            <td>лар</td>
                            <td>лар</td>
                            <td>
                                <button class="btn_edit"></button>
                                <button class="btn_delete"></button>
                            </td>
                        </tr>
                        <tr>
                            <td>لار</td>
                            <td>| 1604 | 1575 | 1585 |</td>
                            <td>лар</td>
                            <td>лар</td>
                            <td>лар</td>
                            <td>лар</td>
                            <td>лар</td>
                            <td>лар</td>
                            <td>лар</td>
                            <td>лар</td>
                            <td>лар</td>
                            <td>лар</td>
                            <td>
                                <button class="btn_edit"></button>
                                <button class="btn_delete"></button>
                            </td>
                        </tr>
                        <tr>
                            <td>لار</td>
                            <td>| 1604 | 1575 |</td>
                            <td>лар</td>
                            <td>лар</td>
                            <td>лар</td>
                            <td>лар</td>
                            <td>лар</td>
                            <td>лар</td>
                            <td>лар</td>
                            <td>лар</td>
                            <td>лар</td>
                            <td>лар</td>
                            <td>
                                <button class="btn_edit"></button>
                                <button class="btn_delete"></button>
                            </td>
                        </tr>
                        <tr>
                            <td>لار</td>
                            <td>| 1604 |</td>
                            <td>лар</td>
                            <td>лар</td>
                            <td>лар</td>
                            <td>лар</td>
                            <td>лар</td>
                            <td>лар</td>
                            <td>лар</td>
                            <td>лар</td>
                            <td>лар</td>
                            <td>лар</td>
                            <td>
                                <button class="btn_edit"></button>
                                <button class="btn_delete"></button>
                            </td>
                        </tr>
                    </table>

                </div>
            </section>

            <?php include('inc/footer.inc.php') ?>

        </div>

        <?php include('inc/scripts.inc.php') ?>

    </body>
</html>
