<!doctype html>
<html class="no-js" lang="">
    <head>
        <?php include('inc/head.inc.php') ?>
    </head>
    <body>


        <div class="page">

            <?php include('inc/header.inc.php') ?>

            <nav class="nav">
                <div class="container">
                    <ul class="nav__menu">
                        <li><a href="#"><span>Проекты</span></a></li>
                        <li class="active"><a href="#"><span>Файлы</span></a></li>
                        <li><a href="#"><span>Рабочая область</span></a></li>
                        <li><a href="#"><span>Редактирование аффиксов</span></a></li>
                        <li><a href="#"><span>Редактирование словаря</span></a></li>
                    </ul>
                </div>
            </nav>


            <section class="main">
                <div class="container">
                    <h1>Современное западное мировоззрение и восточные религии</h1>
                    <div class="main_author"><span>Автор: </span> <a href="#">Белицкая Оксана</a></div>
                    <div class="heading">
                        <div class="heading__title">Загруженные файлы:</div>
                        <div class="heading__actions">
                            <a href="#" class="btn_icon btn_icon_download"></a>
                            <a href="#" class="btn_icon btn_icon_reload"></a>
                        </div>
                    </div>

                    <table class="table">
                        <tr>
                            <th>Наименование</th>
                            <th>Дата создания</th>
                            <th>Статус</th>
                            <th></th>
                        </tr>
                        <tr>
                            <td class="font_medium"><a href="#">Мессианские аспекты нагорной проповеди.</a></td>
                            <td class="text_nowrap">2019-01-13 14:05:02</td>
                            <td class="text_nowrap">Опознаны символы</td>
                            <td>
                                <button class="btn_delete"></button>
                            </td>
                        </tr>
                        <tr>
                            <td class="font_medium"><a href="#">Современное западное мировоззрение и восточные религии</a></td>
                            <td class="text_nowrap">2019-01-13 14:05:02</td>
                            <td class="text_nowrap">Опознаны символы</td>
                            <td>
                                <button class="btn_delete"></button>
                            </td>
                        </tr>
                        <tr>
                            <td class="font_medium"><a href="#">Правовой статус традиционной религии</a></td>
                            <td class="text_nowrap">2019-01-13 14:05:02</td>
                            <td class="text_nowrap">Опознаны символы</td>
                            <td>
                                <button class="btn_delete"></button>
                            </td>
                        </tr>
                        <tr>
                            <td class="font_medium"><a href="#">Свт. Иоанн Златоуст о необходимости апологетики</a></td>
                            <td class="text_nowrap">2019-01-13 14:05:02</td>
                            <td class="text_nowrap">Опознаны символы</td>
                            <td>
                                <button class="btn_delete"></button>
                            </td>
                        </tr>
                        <tr>
                            <td class="font_medium"><a href="#">Богослужебные тексты о еретиках</a></td>
                            <td class="text_nowrap">2019-01-13 14:05:02</td>
                            <td class="text_nowrap">Опознаны символы</td>
                            <td>
                                <button class="btn_delete"></button>
                            </td>
                        </tr>
                        <tr>
                            <td class="font_medium"><a href="#">Смиренный весельчак Амвросий</a></td>
                            <td class="text_nowrap">2019-01-13 14:05:02</td>
                            <td class="text_nowrap">Опознаны символы</td>
                            <td>
                                <button class="btn_delete"></button>
                            </td>
                        </tr>
                        <tr>
                            <td class="font_medium"><a href="#">Истинность наших представлений о Боге</a></td>
                            <td class="text_nowrap">2019-01-13 14:05:02</td>
                            <td class="text_nowrap">Опознаны символы</td>
                            <td>
                                <button class="btn_delete"></button>
                            </td>
                        </tr>
                        <tr>
                            <td class="font_medium"><a href="#">Йога в Греции</a></td>
                            <td class="text_nowrap">2019-01-13 14:05:02</td>
                            <td class="text_nowrap">Опознаны символы</td>
                            <td>
                                <button class="btn_delete"></button>
                            </td>
                        </tr>
                        <tr>
                            <td class="font_medium"><a href="#">Старец Паисий Святогорец о реинкарнации и индуизме</a></td>
                            <td class="text_nowrap">2019-01-13 14:05:02</td>
                            <td class="text_nowrap">Опознаны символы</td>
                            <td>
                                <button class="btn_delete"></button>
                            </td>
                        </tr>
                        <tr>
                            <td class="font_medium"><a href="#">Святейший Патриарх Сербский ПАВЕЛ</a></td>
                            <td class="text_nowrap">2019-01-13 14:05:02</td>
                            <td class="text_nowrap">Опознаны символы</td>
                            <td>
                                <button class="btn_delete"></button>
                            </td>
                        </tr>
                        <tr>
                            <td class="font_medium">Николай Японский о памятнике Пушкину</a></td>
                            <td class="text_nowrap">2019-01-13 14:05:02</td>
                            <td class="text_nowrap">Опознаны символы</td>
                            <td>
                                <button class="btn_delete"></button>
                            </td>
                        </tr>
                        <tr>
                            <td><a href="#">Лидер группы Metallica о своем сектантском детстве</a></td>
                            <td class="text_nowrap">2019-01-13 14:05:02</td>
                            <td class="text_nowrap">Опознаны символы</td>
                            <td>
                                <button class="btn_delete"></button>
                            </td>
                        </tr>
                    </table>

                </div>
            </section>

            <?php include('inc/footer.inc.php') ?>

        </div>

        <?php include('inc/scripts.inc.php') ?>

    </body>
</html>
