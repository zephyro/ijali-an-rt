<!doctype html>
<html class="no-js" lang="">
    <head>
        <?php include('inc/head.inc.php') ?>
    </head>
    <body>


        <div class="page">

            <?php include('inc/header.inc.php') ?>

            <nav class="nav">
                <div class="container">
                    <ul class="nav__menu">
                        <li><a href="#"><span>Проекты</span></a></li>
                        <li><a href="#"><span>Файлы</span></a></li>
                        <li><a href="#"><span>Рабочая область</span></a></li>
                        <li class="active"><a href="#"><span>Редактирование аффиксов</span></a></li>
                        <li><a href="#"><span>Редактирование словаря</span></a></li>
                    </ul>
                </div>
            </nav>


            <section class="main">
                <div class="container">

                </div>
            </section>

            <?php include('inc/footer.inc.php') ?>

        </div>

        <?php include('inc/scripts.inc.php') ?>

    </body>
</html>
