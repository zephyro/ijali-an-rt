<header class="header">
    <div class="container">
        <div class="header__row">
            <a class="header__logo" href="/">
                <div class="header__logo_icon">
                    <img src="img/logo.png" class="img_fluid" alt="">
                </div>
                <div class="header__logo_text">
                    <span>Центр письменного <br/>и музыкального наследия</span>
                    <strong>ИЯЛИ АН РТ</strong>
                </div>
            </a>
            <div class="header__button">
                <a href="#new" class="btn btn_brown btn_modal">Создать проект</a>
                <a href="#" class="btn">Загрузить файл</a>
            </div>
        </div>
    </div>
</header>