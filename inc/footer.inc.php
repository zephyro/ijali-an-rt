<footer class="footer">
    <div class="container">
        <div class="footer__row">
            <div class="footer__text">© www.zafir.ru Все права защищены</div>
        </div>
    </div>
</footer>


<div class="hide">
    <div class="modal" id="new">
        <div class="modal__header"><span>Создание нового проекта</span></div>
        <div class="modal__content">
            <form class="form">
                <div class="form_group">
                    <label class="form_label">Наименование проекта:</label>
                    <input class="form_control" type="text" name="name" placeholder="" value="">
                </div>
                <div class="btn_group">
                    <button type="submit" class="btn btn_brown">Сохранить</button>
                    <button type="button" class="btn btn_text" data-fancybox-close>Отменить</button>
                </div>
            </form>
        </div>
    </div>
</div>