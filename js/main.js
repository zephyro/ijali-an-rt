(function() {

    $('.hide_bar_toggle').on('click touchstart', function(e){
        e.preventDefault();
        $('.hide_bar').slideToggle('fast');
    });

    $('.hide_bar_submit').on('click', function(e){
        e.preventDefault();
        $('.hide_bar').slideUp('fast');
    });
}());

$(".btn_modal").fancybox({
    autoFocus: true
});



(function() {

    $('.board__heading_toggle').on('click touchstart', function(e){
        e.preventDefault();
        $('.board_row').toggleClass('board_hide');
    });
}());



// Avoid `console` errors in browsers that lack a console.
(function() {
    var method;
    var noop = function () {};
    var methods = [
        'assert', 'clear', 'count', 'debug', 'dir', 'dirxml', 'error',
        'exception', 'group', 'groupCollapsed', 'groupEnd', 'info', 'log',
        'markTimeline', 'profile', 'profileEnd', 'table', 'time', 'timeEnd',
        'timeline', 'timelineEnd', 'timeStamp', 'trace', 'warn'
    ];
    var length = methods.length;
    var console = (window.console = window.console || {});

    while (length--) {
        method = methods[length];

        // Only stub undefined methods.
        if (!console[method]) {
            console[method] = noop;
        }
    }
}());


